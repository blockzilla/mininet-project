## mininet+ryu for Blockchain Validator network
There are three main files there:

* [`net.py`](./net.py): the python code used to implement customized network topology using Mininet's python API
* `MiniNAM.py + conf.config`: [MiniNAM](https://www.ucc.ie/en/misl//research/software/mininam/) is based on mininet, and its usage is the same as mininet, but with an additional runtime interface.
* [`scripts/install.sh`](./scripts/install.sh): installs the dependencies to run this project. Mininet, Wireshark, RYU
* `topologies/*.mn`:  progression of the sentry node network topology as defined in Miniedit

# Controllers

Blueprint for further development

```bash
controllers/					# ryu controllers
controllers/cloud-aws.py 			# AWS SDN cloud controller
controllers/cloud-gce.py 			# GCE SDN cloud controller
controllers/datacenter.py 			# Datacenter SDN cloud controller
controllers/hsm.py 				# HSM SDN cloud controller
controllers/vpc-peering.py 			# VPC Peering SDN cloud controller
```

### Cloud Controller Requirements
* Implement cloud specifc rules.
* Flow controll between sentry nodes and datacenter
* Limit validator - cloud relation ship
* Cloud peering, GCE -> AWS
* Failover to Disaster recovery, if link to datacenter faiis

### Datacenter Controller Requirements
* Flow controll between datacenter and sentry nodes
* Failover to Disaster recovery Validator

### HSM Controller Requirements
* Limit validator connections to backup HSM unless failover scenario
* Failover to Disaster recovery HSM
* Validator Double Sign prevention

# How to run

1. We can open a terminal to start controller process with the command below. The *--observe-links* should be added to discover the topology, otherwise no link information can be seen.


Run all controllers. [note issues with connecting mininet to multiple remotes, must get ports correct for local controllers] example: `net.py`

```python
hsm2 = net.addController(name='hsm2', controller=RemoteController,protocol='tcp',ip='127.0.0.1',port=6636)
```


```bash
mn -c
sudo ryu-manager ../controllers/cloud-gce.py --observe-links &
sudo ryu-manager ../controllers/hsm1.py --observe-links &
sudo ryu-manager ../controllers/hsm2.py --observe-links &
sudo ryu-manager ../controllers/datacenter.py --observe-links &
```

Run Shortest path controller

```bash
mn -c
sudo ryu-manager sp.py --observe-links
```

	In order to use default mininet controller, execute nothing for step 1

2. We can open another terminal with the command below to run MiniNAM to start Mininet and create a customized network. The * --controller = remote* parameter is used to connect the started controller process of Ryu.

	```bash
	sudo python MiniNAM.py --custom net.py --topo ValidatorTopo --controller=remote
	```

	![image.png](./img.png)

3. We will have a network shown above, and we can test the some of the network connectivity between nodes

	```bash
	mininet>node1 ping -c 4 node2
	mininet>node1 ping -c sentry1
	mininet>validator1 ping -c 4 node1
	mininet>sentry1 ping -c 2 validator1
	mininet>sentry2 ping -c 2 validator3
	mininet>hsm1 ping -c 2 validator1
	mininet>hsm1 ping -c 2 validator2
	```


# Further Reading

Potentially use Docker to run Mininet simulations, could be a goo way to use remote controller but generating a Docker compose of kubernetes pod with controllers / services

Containernet is a fork of the famous Mininet network emulator and allows to use Docker containers as hosts in emulated network topologies
https://github.com/containernet/containernet

```
d1 = net.addDocker('d1', ip='10.0.0.251', dimage="ubuntu:trusty")
d2 = net.addDocker('d2', ip='10.0.0.252', dimage="ubuntu:trusty")
```

#### Idea Mine
- [Blockchain-Enabled SDN: Evolving Network Security Parameters](https://gomindsight.com/insights/blog/blockchain-enabled-sdn/)
- [BSS: Blockchain security over software defined network](https://ieeexplore.ieee.org/document/8229910)
- [Deployment of Blockchain Technology in Software Defined Networks: A Survey](https://ieeexplore.ieee.org/document/8952698)
- [SECURE SOFTWARE-DEFINED NETWORKING BASED ON BLOCKCHAIN](https://arxiv.org/pdf/1906.04342v1.pdf)