#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call


def myNetwork():

    net = Mininet(topo=None,
                  build=False,
                  ipBase='10.0.0.0/8')

    info('*** Adding controller\n')
    cloudcontroller1 = net.addController(name='cloudcontroller1',
                                         controller=Controller,
                                         protocol='tcp',
                                         port=6633)

    c3 = net.addController(name='c3',
                           controller=Controller,
                           protocol='tcp',
                           port=6633)

    c1 = net.addController(name='c1',
                           controller=Controller,
                           protocol='tcp',
                           port=6634)

    c4 = net.addController(name='c4',
                           controller=Controller,
                           protocol='tcp',
                           port=6633)

    info('*** Add switches\n')
    hsmswitch3 = net.addSwitch('hsmswitch3', cls=OVSKernelSwitch)
    cloudvpc1 = net.addSwitch('cloudvpc1', cls=OVSKernelSwitch)
    drswitch1 = net.addSwitch('drswitch1', cls=OVSKernelSwitch)
    euwest2 = net.addSwitch('euwest2', cls=OVSKernelSwitch)
    useast1 = net.addSwitch('useast1', cls=OVSKernelSwitch)
    dcswitch2 = net.addSwitch('dcswitch2', cls=OVSKernelSwitch)
    s10 = net.addSwitch('s10', cls=OVSKernelSwitch, failMode='standalone')
    euwest1 = net.addSwitch('euwest1', cls=OVSKernelSwitch)
    hsmswitch1 = net.addSwitch('hsmswitch1', cls=OVSKernelSwitch)
    northeast1 = net.addSwitch('northeast1', cls=OVSKernelSwitch)
    dcswitch1 = net.addSwitch('dcswitch1', cls=OVSKernelSwitch)
    s12 = net.addSwitch('s12', cls=OVSKernelSwitch, failMode='standalone')
    hsmswitch2 = net.addSwitch('hsmswitch2', cls=OVSKernelSwitch)
    s2 = net.addSwitch('s2', cls=OVSKernelSwitch, failMode='standalone')

    info('*** Add hosts\n')
    sentry2 = net.addHost('sentry2', cls=Host,
                          ip='10.0.0.5', defaultRoute=None)
    validator3 = net.addHost('validator3', cls=Host,
                             ip='10.0.0.10', defaultRoute=None)
    sentry1 = net.addHost('sentry1', cls=Host,
                          ip='10.0.0.4', defaultRoute=None)
    hsm1 = net.addHost('hsm1', cls=Host, ip='10.0.0.13', defaultRoute=None)
    hsm2 = net.addHost('hsm2', cls=Host, ip='10.0.0.12', defaultRoute=None)
    node3 = net.addHost('node3', cls=Host, ip='10.0.0.3', defaultRoute=None)
    sentry3 = net.addHost('sentry3', cls=Host,
                          ip='10.0.0.6', defaultRoute=None)
    node2 = net.addHost('node2', cls=Host, ip='10.0.0.2', defaultRoute=None)
    validator1 = net.addHost('validator1', cls=Host,
                             ip='10.0.0.8', defaultRoute=None)
    peer1 = net.addHost('peer1', cls=Host, ip='10.0.0.11', defaultRoute=None)
    hsm4 = net.addHost('hsm4', cls=Host, ip='10.0.0.11', defaultRoute=None)
    validator2 = net.addHost('validator2', cls=Host,
                             ip='10.0.0.9', defaultRoute=None)
    node1 = net.addHost('node1', cls=Host, ip='10.0.0.1', defaultRoute=None)

    info('*** Add links\n')
    net.addLink(s2, useast1)
    net.addLink(s2, northeast1)
    net.addLink(euwest2, drswitch1)
    net.addLink(drswitch1, validator3)
    net.addLink(dcswitch1, validator1)
    net.addLink(dcswitch2, validator2)
    net.addLink(cloudvpc1, dcswitch1)
    net.addLink(cloudvpc1, dcswitch2)
    net.addLink(useast1, sentry1)
    net.addLink(euwest1, sentry2)
    net.addLink(s2, cloudvpc1)
    net.addLink(northeast1, sentry3)
    net.addLink(node1, s12)
    net.addLink(node2, s12)
    net.addLink(node3, s12)
    net.addLink(s12, s2)
    net.addLink(euwest2, s2)
    net.addLink(s2, euwest1)
    net.addLink(northeast1, s10)
    net.addLink(s10, peer1)
    net.addLink(validator1, hsmswitch1)
    net.addLink(validator2, hsmswitch2)
    net.addLink(hsmswitch2, hsm2)
    net.addLink(hsmswitch1, hsm1)
    net.addLink(validator3, hsmswitch3)
    net.addLink(hsmswitch3, hsm4)
    net.addLink(hsmswitch1, hsmswitch2)

    info('*** Starting network\n')
    net.build()
    info('*** Starting controllers\n')
    for controller in net.controllers:
        controller.start()

    info('*** Starting switches\n')
    net.get('hsmswitch3').start([c4])
    net.get('cloudvpc1').start([cloudcontroller1])
    net.get('drswitch1').start([c1])
    net.get('euwest2').start([cloudcontroller1])
    net.get('useast1').start([cloudcontroller1])
    net.get('dcswitch2').start([c1])
    net.get('s10').start([])
    net.get('euwest1').start([cloudcontroller1])
    net.get('hsmswitch1').start([c3])
    net.get('northeast1').start([cloudcontroller1])
    net.get('dcswitch1').start([c1])
    net.get('s12').start([c3])
    net.get('hsmswitch2').start([c3])
    net.get('s2').start([])

    info('*** Post configure switches and hosts\n')

    CLI(net)
    net.stop()


if __name__ == '__main__':
    setLogLevel('info')
    myNetwork()
