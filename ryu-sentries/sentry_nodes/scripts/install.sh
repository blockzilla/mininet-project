echo "Install wireshark and mininet"
sudo add-apt-repository ppa:wireshark-dev/stable
sudo apt update
sudo apt upgrade -y
sudo apt install mininet wireshark xterm git hping3 -y
mn
wireshark --version

echo "Configure wireshark"
sudo usermod -a -G wireshark $USER
sudo chgrp wireshark /usr/bin/dumpcap

sudo chmod 750 /usr/bin/dumpcap
sudo setcap cap_net_raw,cap_net_admin=eip /usr/bin/dumpcap
sudo getcap /usr/bin/dumpcap

git clone https://github.com/mininet/mininet
cd mininet/utils
chmod +x ./install.sh
install.sh -a


# ryu
sudo apt update
sudo apt install python-pip -Y
pip install -r tools/optional-requires
git clone git://github.com/osrg/ryu.git
cd ryu; pip install .

