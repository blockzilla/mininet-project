


#!/usr/bin/python

from mininet.topo import Topo

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call
from mininet.util import dumpNodeConnections


class ValidatorTopo(Topo):
    def __init__(self, **opts):
        Topo.__init__(self, **opts)

        info('*** Add switches\n')
        useast1 = self.addSwitch('useast1', cls=OVSKernelSwitch)
        cloudvpc1 = self.addSwitch('cloudvpc1', cls=OVSKernelSwitch)
        drswitch1 = self.addSwitch('drswitch1', cls=OVSKernelSwitch)
        euwest2 = self.addSwitch('euwest2', cls=OVSKernelSwitch)
        dcswitch2 = self.addSwitch('dcswitch2', cls=OVSKernelSwitch)
        peervpc1 = self.addSwitch('peervpc1', cls=OVSKernelSwitch)
        euwest1 = self.addSwitch('euwest1', cls=OVSKernelSwitch)
        northeast1 = self.addSwitch('northeast1', cls=OVSKernelSwitch)
        dcswitch1 = self.addSwitch('dcswitch1', cls=OVSKernelSwitch)
        hsmswitch1 = self.addSwitch('hsmswitch1', cls=OVSKernelSwitch)
        hsmswitch2 = self.addSwitch('hsmswitch2', cls=OVSKernelSwitch)
        hsmswitch3 = self.addSwitch('hsmswitch3', cls=OVSKernelSwitch)
        s12 = self.addSwitch('s12', cls=OVSKernelSwitch, failMode='standalone')
        s2 = self.addSwitch('s2', cls=OVSKernelSwitch, failMode='standalone')

        info('*** Add hosts\n')
        # Add Default Routes
        sentry2 = self.addHost('sentry2', cls=Host, ip='10.0.0.5', defaultRoute=None)
        validator3 = self.addHost('validator3', cls=Host, ip='10.0.0.10', defaultRoute=None)
        sentry1 = self.addHost('sentry1', cls=Host, ip='10.0.0.4', defaultRoute=None)
        node3 = self.addHost('node3', cls=Host, ip='10.0.0.3', defaultRoute=None)
        sentry3 = self.addHost('sentry3', cls=Host, ip='10.0.0.6', defaultRoute=None)
        node2 = self.addHost('node2', cls=Host, ip='10.0.0.2', defaultRoute=None)
        validator1 = self.addHost('validator1', cls=Host, ip='10.0.0.8', defaultRoute=None)
        peer1 = self.addHost('peer1', cls=Host, ip='10.0.0.11', defaultRoute=None)
        validator2 = self.addHost('validator2', cls=Host, ip='10.0.0.9', defaultRoute=None)
        node1 = self.addHost('node1', cls=Host, ip='10.0.0.1', defaultRoute=None)
        hsm1 = self.addHost('hsm1', cls=Host, ip='10.0.0.13', defaultRoute=None)
        hsm2 = self.addHost('hsm2', cls=Host, ip='10.0.0.12', defaultRoute=None)
        hsm4 = self.addHost('hsm4', cls=Host, ip='10.0.0.11', defaultRoute=None)


        info('*** Add links\n')
        self.addLink(s2, useast1)
        self.addLink(s2, northeast1)
        self.addLink(euwest2, drswitch1)
        self.addLink(drswitch1, validator3)
        self.addLink(dcswitch1, validator1)
        self.addLink(dcswitch2, validator2)
        self.addLink(cloudvpc1, dcswitch1)
        self.addLink(cloudvpc1, dcswitch2)
        self.addLink(useast1, sentry1)
        self.addLink(euwest1, sentry2)
        self.addLink(s2, cloudvpc1)
        self.addLink(northeast1, sentry3)
        self.addLink(node1, s12)
        self.addLink(node2, s12)
        self.addLink(node3, s12)
        self.addLink(s12, s2)
        self.addLink(euwest2, s2)
        self.addLink(northeast1, peervpc1)
        self.addLink(peervpc1, peer1)
        self.addLink(s2, euwest1)
        self.addLink(validator1, hsmswitch1)
        self.addLink(validator2, hsmswitch2)
        self.addLink(hsmswitch2, hsm2)
        self.addLink(hsmswitch1, hsm1)
        self.addLink(validator3, hsmswitch3)
        self.addLink(hsmswitch3, hsm4)
        self.addLink(hsmswitch1, hsmswitch2)

class DefaultSwitch(Topo):
    "Single switch connected to n hosts."

    def __init__(self, n=2, **opts):
        # Initialize topology and default options
        Topo.__init__(self, **opts)
        switch = self.addSwitch('s1')
        # Python's range(N) generates 0..N-1
        for h in range(n):
            host = self.addHost('h%s' % (h + 1))
            self.addLink(host, switch)


def ValidatorTest():
    "Create and test a simple network"
    # topo = DefaultSwitch(n=4)
    topo = Topo.__init__(self, **opts)
    net = Mininet(topo=topo,
                build=False,
                ipBase='10.0.0.0/8')

    # TODO: containernet might make this easier
    info('*** Adding controller\n')

    # Use local remote controllers 127.0.0.1
    cloudcontroller1 = net.addController(name='cloudcontroller1',
                                            controller=RemoteController,
                                            ip='127.0.0.1',
                                            protocol='tcp',
                                            port=6633)
    dataceneter = net.addController(name='dataceneter',
                            controller=RemoteController,
                            protocol='tcp',
                            ip='127.0.0.1',
                            port=6634)
    hsm1 = net.addController(name='hsm1',
                            controller=RemoteController,
                            protocol='tcp',
                            ip='127.0.0.1',
                            port=6635)
    hsm2 = net.addController(name='hsm2',
                            controller=RemoteController,
                            protocol='tcp',
                            ip='127.0.0.1',
                            port=6636)

    net.start()
    print "Dumping host connections"
    print "Testing network connectivity"
    info('*** Starting network\n')
    net.build()
    info('*** Starting controllers\n')
    for controller in net.controllers:
        controller.start()

    info('*** Starting switches\n')
    net.get('useast1').start([cloudcontroller1])
    net.get('cloudvpc1').start([cloudcontroller1])
    net.get('drswitch1').start([dataceneter])
    net.get('euwest2').start([cloudcontroller1])
    net.get('dcswitch2').start([dataceneter])
    net.get('peervpc1').start([cloudcontroller1])
    net.get('euwest1').start([cloudcontroller1])
    net.get('northeast1').start([cloudcontroller1])
    net.get('dcswitch1').start([dataceneter])
    net.get('s12').start([])
    net.get('s2').start([])
    net.get('hsmswitch3').start([hsm2])
    net.get('hsmswitch1').start([hsm1])
    net.get('hsmswitch2').start([hsm1])

    info('*** Post configure switches and hosts\n')
    dumpNodeConnections(net.hosts)
    print "Testing network connectivity"
    CLI(net)
    net.pingAll()
    net.stop()

locations = {
    'cloudcontroller1':(149,533),
    'c2':(1054,237),
    'c1':(788.0,695.0),
    'sentry1':(69.0,351.0),
    'sentry2':(280.0,411.0),
    'sentry3':(644.0,342.0),
    'peer1':(862.0,359.0),
    'node1':(333.0,52.0),
    'node2':(438.0,36.0),
    'node3':(538.0,56.0),
    's12':(441.0,121.0),
    's2':(442.0,239.0),
    'useast1':(111.0,239.0),
    'euwest1':(279.0,311.0),
    'euwest2':(636.0,469.0),
    'northeast1':(643.0,238.0),
    'peervpc1':(861.0,239.0),
    'cloudvpc1':(441.0,409.0),
    'dcswitch1':(373.0,574.0),
    'dcswitch2':(545.0,572.0),
    'drswitch1':(1012.0,469.0),
    'validator1':(373.0,669.0),
    'validator2':(546.0,671.0),
    'validator3':(1011.0,608.0),
    'hsmswitch1':(373.0,762.0),
    'hsmswitch2':(549.0,762.0),
    'hsmswitch3':(1012.0,707.0),
    'hsm1':(374.0,857.0),
    'hsm2':(550.0,856.0),
    'hsm4':(1012.0,815.0),
    }

topos = {'ValidatorTopo': ValidatorTopo}

if __name__ == '__main__':
    # Tell mininet to print useful information
    setLogLevel('info')
    ValidatorTest()
